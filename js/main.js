(function(){
    "use strict";
    var regalo = document.getElementById('regalo');
    
    if (document.getElementById('mapa')) {
 
        // colocar tu código del mapa
        var map = L.map('mapa').setView([6.276652, -75.553451], 17);
        L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);

L.marker([6.276652, -75.553451]).addTo(map)
    .bindPopup('GdlWebCamp 2020.<br>Boletos ya Disponible.')
    .openPopup();
       };

    document.addEventListener('DOMContentLoaded', function(){
    // Campos Datos usuarios
    var nombre = document.getElementById('nombre');
    var apellido = document.getElementById('apellido');
    var email = document.getElementById('email');

     //campos pases
    var pase_dia = document.getElementById('pase_dia');
    var pase_dosdias = document.getElementById('pase_dosdias');
    var pase_completo = document.getElementById('pase_completo');
    
    //Botones y Divs
    var calcular = document.getElementById('calcular');
    var errorDiv = document.getElementById('error');
    var botonRegistro = document.getElementById('btnRegistro');
    var lista_productos = document.getElementById('lista-productos');
    var suma = document.getElementById('suma-total');
    
    // Extras
    var camisas = document.getElementById('camisa_evento');
    var etiquetas = document.getElementById('etiquetas');
    
    botonRegistro.disabled = true;

    if(document.getElementById('calcular')){
    calcular.addEventListener('click', calcularMontos);
    
    // pase_dia.addEventListener('blur', mostrarDias);
    // pase_dosdias.addEventListener('blur', mostrarDias);
    // pase_completo.addEventListener('blur', mostrarDias);
    pase_dia.addEventListener("input", mostrarDias);
    pase_dosdias.addEventListener("input", mostrarDias);
    pase_completo.addEventListener("input", mostrarDias); 
    pase_dia.addEventListener('propertychange', mostrarDias); // for IE8  
    pase_dosdias.addEventListener('propertychange', mostrarDias); // for IE8 
    pase_completo.addEventListener('propertychange', mostrarDias); // for IE8
    
    nombre.addEventListener('blur', validarCampos);
    apellido.addEventListener('blur', validarCampos);
    email.addEventListener('blur', validarCampos);
    email.addEventListener('blur', validarMail);
        function validarCampos(){
            if(this.value == ''){
                errorDiv.style.display = 'block';
                errorDiv.innerHTML = "este campo es obligatorio";
                this.style.border = '1px solid red';
                errorDiv.style.border = ' 1px solid red' ;
            }else{
                errorDiv.style.display = 'none';
                this.style.border = '1px solid #cccccc';
            }
        }

        function validarMail(){
            if(this.value.indexOf("@") > -1){
                errorDiv.style.display = 'none';
                this.style.border = '1px solid #cccccc';
            }else{
                errorDiv.style.display = 'block';
                errorDiv.innerHTML = "debe de tener por lo menos una @";
                this.style.border = '1px solid red';
                errorDiv.style.border = ' 1px solid red' ;
            }
        }
    
    
    function calcularMontos(event){
        event.preventDefault();
        if(regalo.value === ''){
            alert("debes elegir un regalo");
            regalo.focus();
        }else{
            var boletoDia = parseInt(pase_dia.value, 10) || 0,
                boleto2Dias = parseInt(pase_dosdias.value , 10) || 0,
                boletoCompleto = parseInt(pase_completo.value, 10) || 0,
                cantCamisas = parseInt(camisas.value, 10) || 0,
                cantEtiquetas = parseInt(etiquetas.value, 10) || 0;

                var totalPagar = (boletoDia * 30) + (boleto2Dias * 45) +(boletoCompleto * 50) + ((cantCamisas * 10) * .93) + (cantEtiquetas * 2) ;
                
                var listadosProductos = [];
                if(boletoDia >= 1){
                    listadosProductos.push(boletoDia + 'pases por día');
                }
                if(boleto2Dias >= 1){
                    listadosProductos.push(boleto2Dias + 'pases por 2 días');
                }
                if(boletoCompleto >= 1){
                    listadosProductos.push(boletoCompleto + 'pases completo');
                }
                if(cantCamisas >= 1){
                    listadosProductos.push(cantCamisas + 'camisas');
                }
                if(cantEtiquetas >= 1){
                    listadosProductos.push(cantEtiquetas + 'etiquetas');
                }
                lista_productos.style.display = "block";
                lista_productos.innerHTML = ''; 

                for(var i = 0; i < listadosProductos.length; i++){
                    lista_productos.innerHTML += listadosProductos[i] + '<br/>';
                }
                suma.innerHTML = "$ " + totalPagar.toFixed(2);

                botonRegistro.disabled = false;
                document.getElementById('total_pedido').value = totalPagar;
        }  
    }
    function mostrarDias(){
        var boletoDia = parseInt(pase_dia.value, 10) || 0,
            boleto2Dias = parseInt(pase_dosdias.value , 10) || 0,
            boletoCompleto = parseInt(pase_completo.value, 10) || 0;
            
            
            var diasElegidos = [];             document.getElementsByClassName("contenido-dia")[0].style.display = "none";             document.getElementsByClassName("contenido-dia")[1].style.display = "none";             document.getElementsByClassName("contenido-dia")[2].style.display = "none";
            if(boletoDia > 0){
                diasElegidos.push('viernes')
            }
            if(boleto2Dias > 0){
                diasElegidos.push('viernes', 'sabado')
            }
            if(boletoCompleto > 0){
                diasElegidos.push('viernes', 'sabado', 'domingo')
            }

            for(var i = 0; i < diasElegidos.length; i++){
                document.getElementById(diasElegidos[i]).style.display = "block";
            }
    }
    
    }

    });//DOM content loaded
})();


$(function(){
    
    //lettering
    $('.nombre-sitio').lettering();

    $('body.conferencia .navegacion-principal a:contains("Conferencia")').addClass('activo');
    $('body.calendario .navegacion-principal a:contains("Calendario")').addClass('activo');
    $('body.invitados .navegacion-principal a:contains("Invitados")').addClass('activo');

    //menu fijo

    var windowHeight = $(window).height();
    var barraAltura = $('.barra').innerHeight();

    $(window).scroll(function(){
        var scroll = $(window).scrollTop();
        if(scroll > windowHeight){
            $('.barra').addClass('fixed');
            $('body').css({'margin-top': barraAltura+'px'});
        }else{
            $('.barra').removeClass('fixed');
            $('body').css({'margin-top': '0px'});
        }
    });

    //menu responsive

    $('.menu-movil').on('click', function(){
        $('.navegacion-principal').slideToggle();
    });



    //programa de conferencias
    $('.programa-evento .info-curso:first').show();
    $('.menu-programa a:first').addClass('activo');
  
    $('.menu-programa a').on('click', function(){
        $('.menu-programa a').removeClass('activo');
        $(this).addClass('activo');
        $('.ocultar').hide();
        var enlace = $(this).attr('href');
        $(enlace).fadeIn(1000);
        return false;
    });
    
    


    //animaciones para numeros
    $('.resumen-evento li:nth-child(1) p').animateNumber({number: 6}, 1200);
    $('.resumen-evento li:nth-child(2) p').animateNumber({number: 15}, 1200);
    $('.resumen-evento li:nth-child(3) p').animateNumber({number: 3}, 2000);
    $('.resumen-evento li:nth-child(4) p').animateNumber({number: 9}, 1500);
    
    //cuenta regresiva
    $('.cuenta-regresiva').countdown('2021/04/16 09:00:00', function(event){
        $('#dias').html(event.strftime('%D'));
        $('#horas').html(event.strftime('%H'));
        $('#minutos').html(event.strftime('%M'));
        $('#segundos').html(event.strftime('%S'));
    });

    //colorbox
    $('.invitado-info').colorbox({inline:true, width:"50%"});
  
  });